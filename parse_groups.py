#!/usr/env python3

import requests
import json

access_token = '8aa884cf4c6e10ceed66fd9914d0d35c2531c1bc9b6cb4bb3fec68e8a9a59a78dc9965b8dab446fdafba3'
v = 5.63

def parse_data_from_file(file):
    with open(file, "r") as filestream:
        for line in filestream:
            currentline = line.split(",")
        return currentline

def unique(lst):
    seen = set()
    result = []
    for x in lst:
        if x in seen:
            continue
        seen.add(x)
        result.append(x)
    return result


def get_user_groups():
    offset = 0
    all_groups = []
    users = parse_data_from_file("users.txt")
    for user_id in users:
        r = requests.get('https://api.vk.com/method/users.getSubscriptions?', params={'access_token':access_token, 'extended': 1, 'user_id': int(user_id), 'count': 1, 'offset': 0, 'v': v}).json()
        group_count = r['response']['count']
        while True:
            r = requests.get('https://api.vk.com/method/users.getSubscriptions?', params={'access_token':access_token, 'extended': 1, 'user_id': int(user_id), 'count': 200, 'offset': offset, 'v': v}).json()
            try:
                groups = r['response']['items']
                for group in groups:
                    all_groups.append(group['id'])
                    print(group['id'])
            except:
                print("No groups in user")
            if group_count < offset:
                break   
            offset +=100
    return all_groups

def global_search():
    offset = 0
    

def main():
    data = unique(get_user_groups())
    
    file = open("groups.txt","w") 
    file.write(str(data)) 
    file.close() 


if __name__ == '__main__':
    main()
